﻿using Microsoft.Xna.Framework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PacMan
{
    /// <summary>
    /// Author : Danny Manzato-Tates
    /// </summary>
    public class Maze
    {
        /// <summary>
        /// Variable and event
        /// </summary>
        private Tile[,] maze;
        public event Action<string> PacmanWon;

        //property Length of the Tile[,]
        public int Size { get; set; }

        /// <summary>
        /// No parameter constructor
        /// </summary>
        public Maze()
        {

        }

        /// <summary>
        /// Sets the maze and size
        /// </summary>
        /// <param name="maze">The maze array</param>
        public void SetTiles(Tile[,] maze)
        {
            this.maze = maze;
            this.Size = maze.GetLength(0);
        }

        /// <summary>
        /// Indexer with a getter and setter
        /// </summary>
        /// <param name="x">The X coordinate on the maze</param>
        /// <param name="y">The Y coordinate on the maze</param>
        public Tile this[int x, int y]
        {
            get
            {
                if (x < 0 || x >= Size || y < 0 || y >= Size)
                    throw new ArgumentOutOfRangeException("The indeces are out of range (" + x + ", " + y + ")");
                return maze[x, y];
            }
            set
            {
                maze[x, y] = value;
            }
        }

        /// <summary>
        /// Method that gets all available neighouring Tiles of the current position if it can enter
        /// and adds it into a List
        /// </summary>
        /// <param name="position">The current vector</param>
        /// <param name="dir">The current direction</param>
        public List<Tile> GetAvailableNeighbours(Vector2 position, Direction dir)
        {
            List<Tile> t = new List<Tile>();
            Tile left; 
            if ((int)position.X - 1 >= 0 && (int)position.X - 1 < Size)
            {
                left = maze[(int)position.X - 1, (int)position.Y];
            }else
            {
                left = new Wall(0,0);
            }
            Tile right;
            if ((int)position.X + 1 >= 0 && (int) position.X  + 1 < Size)
            {
                right = maze[(int)position.X + 1, (int)position.Y];
            }else
            {
                right = new Wall(0, 0);
            }
            Tile down;
            if((int)position.Y + 1 >= 0 && (int)position.Y + 1 < Size){
                down = maze[(int)position.X, (int)position.Y + 1];
            }else
            {
                down = new Wall(0, 0);
            }
            Tile up;
            if ((int)position.Y - 1 >= 0 && (int)position.Y - 1 < Size)
            {
                up = maze[(int)position.X, (int)position.Y - 1];
            }else
            {
                up = new Wall(0,0);
            }


            switch (dir)
            {
                case Direction.Left:
                    if (left.CanEnter())
                        t.Add(left);
                    if (up.CanEnter())
                        t.Add(up);
                    if (down.CanEnter())
                        t.Add(down);
                    break;
                case Direction.Right:
                    if (right.CanEnter())
                        t.Add(right);
                    if (up.CanEnter())
                        t.Add(up);
                    if (down.CanEnter())
                        t.Add(down);
                    break;
                case Direction.Up:
                    if (right.CanEnter())
                        t.Add(right);
                    if (up.CanEnter())
                        t.Add(up);
                    if (left.CanEnter())
                        t.Add(left);
                    break;
                case Direction.Down:
                    if (right.CanEnter())
                        t.Add(right);
                    if (down.CanEnter())
                        t.Add(down);
                    if (left.CanEnter())
                        t.Add(left);
                    break; 
            }

            return t;
        }

        /// <summary>
        /// Checks if all the tiles are empty, if it is the case, calls 
        /// the event PacmanWon
        /// </summary>
        public bool CheckMembersLeft()
        {
            if (areMembersNull())
            {
                OnPacmanWon();
                return true;
            }
            return false;
        }

        /// <summary>
        /// Event raise when Pacman wins
        /// </summary>
        protected void OnPacmanWon()
        {
            //temporary for this phase
            PacmanWon?.Invoke("win");
        }

        /// <summary>
        /// Helper method that checks if all the Tiles are empty
        /// </summary>
        private bool areMembersNull()
        {
            foreach (Tile t in maze)
            {
                if (t is Path)
                {
                    if (!t.isEmpty())
                    {
                        //Console.WriteLine(t.Position + " is not null" );
                        return false;
                    }
                }
            }

            return true;
        }
        
    }
}
