﻿using Microsoft.Xna.Framework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Timers;

namespace PacMan
{
    /// <summary>
    /// Author : Danny Manzato-Tates
    /// </summary>
    public class Ghost : ICollidable, IMoveable
    {
        /// <summary>
        /// Variables for the Ghost class
        /// </summary>
        private Pacman pacman;//used to update the target
        private Vector2 target;//used in chasing mode
        private Pen pen;//tiles in which the ghosts are contained when they are penned 
        private Maze maze;
        private Direction direction;
        private Vector2 position;//ghosts position
        private string identifier;
        private IGhostState currentState;
        private Color colour;


        //static property
        public static Timer scared;
        public static Vector2 ReleasePosition;//used for release

        //Events
        public event Action PacmanDied;
        public event Action<ICollidable> Collision;

        //Properties
        public Vector2 Position { get { return this.position; } set { this.position = value; } }
        public Direction Direction { get { return this.direction; }set { this.direction = value;} }
        public GhostState CurrentState { get; private set; }
        public int Points { get; set; }
        public Pacman Pacman { get { return pacman; } }
        public String Identifier { get { return identifier; } set { identifier = value; } }
        public int Frame { get; set; }
        public Color Colour { get { return colour; } }

        /// <summary>
        /// Constructor instantiates the gamestate, the location, the target, the ghoststate and color
        /// of the ghost.
        /// </summary>
        public Ghost(GameState g, int x, int y, Vector2 target, GhostState start, Color colour)
        {
            this.target = target;
            this.colour = colour;
            this.pen = g.Pen;
            this.maze = g.Maze;
            this.pacman = g.Pacman;
            this.position = new Vector2(x, y);
            Frame = 0;

            ChangeState(start);
        }

        /// <summary>
        /// Puts all the ghosts into Pen.
        /// </summary>
        public void Reset(bool isGameOver = false)
        {
            pen.AddToPen(this, isGameOver);
        }

        /// <summary>
        /// Changes the state of a ghost
        /// </summary>
        /// <param name="state">The state in which the ghost is changed to</param>
        public void ChangeState(GhostState state)
        {
            GhostState temp = CurrentState;
            //can't change from zombie unless it is released from pen 
            if (!(CurrentState == GhostState.Zombie) || state == GhostState.Released)
            {
                this.CurrentState = state;
                switch (state)
                {
                    case GhostState.Scared:
                        currentState = new Scared(this, this.maze);
                        ////Console.WriteLine(Colour + "change from " + temp + " to " + GhostState.Scared);
                        break;
                    case GhostState.Chase:
                        currentState = new Chase(this, maze, target);
                        ////Console.WriteLine(Colour + "change from " + temp + " to " + GhostState.Chase);
                        break;
                    case GhostState.Released:
                        position = ReleasePosition;
                        currentState = new Chase(this, maze, target);
                        this.CurrentState = GhostState.Chase;
                        ////Console.WriteLine(Colour + "change from " + temp + " to Release to " + GhostState.Chase);
                        break;
                    case GhostState.Zombie:
                        this.currentState = new Zombie(this, Ghost.ReleasePosition, maze);
                        break;
                }
            }
            
        }

        /// <summary>
        /// Method that moves the ghost towards pacman
        /// </summary>
        public void Move()
        {
            //Changes Target positions once it passes the previous target
            if(target == position)
            {
                target = pacman.Position;
                if (CurrentState == GhostState.Chase)
                {
                    this.currentState = new Chase(this,maze,target);
                }
            }
            
            if(maze[(int)this.position.X, (int)this.position.Y] is Teleporter)
            {
                ((Teleporter)maze[(int)this.position.X, (int)this.position.Y]).Entering(this);
            }

            //Use the state object to decide how to move
            ////Console.WriteLine("Is at: " + position + "and goes to " + target);
            currentState.Move();

            if(this.CurrentState == GhostState.Zombie)
            {
                if (this.position == ReleasePosition)
                {
                    Reset();
                }
            }
        }

        

        /// <summary>
        /// Check whether the collision with pacman was scared or chase
        /// </summary>
        public void Collide()
        {
            if (CurrentState == GhostState.Scared)
            {
                //Console.WriteLine("When " + Colour + " collided with pacman he was scared and resets");
                OnCollision(this);
                ChangeState(GhostState.Zombie);
            }
            else if(CurrentState == GhostState.Chase)
            {
                //Console.WriteLine("When " + Colour + " collided with pacman, pacman died");
                OnPacmanDied();
            }
        }

        /// <summary>
        /// Event handler for pacman's death
        /// </summary>
        protected void OnPacmanDied()
        {
            //Console.WriteLine("pacman died event is raised");
            PacmanDied?.Invoke();
        }

        /// <summary>
        /// Event handler for collision
        /// </summary>
        /// <param name="i">The ICollidable that collides with the ghost</param>
        protected void OnCollision(ICollidable i)
        {
            Collision?.Invoke(i);
        }
    }
}
