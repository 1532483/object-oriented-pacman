﻿using Microsoft.Xna.Framework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PacMan
{
    public class Teleporter : Tile
    {
        public Teleporter otherTeleporter { get; set; }
        private Maze maze;

        public override ICollidable Member
        {
            get { return null; }

            set { }
        }

        /// <summary>
        /// Constructor instantiates the position in the base and the maze
        /// </summary>
        public Teleporter(int x, int y, Maze maze) : base(x,y)
        {
            this.maze = maze;
        }


        /// <summary>
        /// The element defines whether anything can enter the tile.
        /// </summary>
        /// <returns>Returns true</returns>
        public override bool CanEnter()
        {
            return true;
        }

        /// <summary>
        /// Calls the member's collide method and deletes the member afterwards
        /// **if something were to collide with the teleporter it does nothing
        /// </summary>
        public override void Collide()
        {
        }

        /// <summary>
        /// The element defines whether there is a member within it.
        /// </summary>
        /// <returns>Returns true</returns>
        public override bool isEmpty()
        {
            return true;
        }


        /// <summary>
        /// Is called to enter the portal and exit from the other portal
        /// </summary>
        /// <param name="p"></param>
        /// <param name="dir"></param>
        public void Entering(Pacman p, Direction dir)
        {
            p.Position = otherTeleporter.Position;
            otherTeleporter.Exiting(p,dir);
            
        }

        /// <summary>
        /// Is called to enter the portal and exit from the other portal
        /// </summary>
        /// <param name="g"></param>
        public void Entering(Ghost g)
        {
            g.Position = otherTeleporter.Position;
            otherTeleporter.Exiting(g);
        }


        /// <summary>
        /// Is called to exit the portal without calling the move method
        /// which could crash the game
        /// </summary>
        /// <param name="p"></param>
        /// <param name="dir"></param>
        private void Exiting(Pacman p, Direction dir)
        {
            List<Tile> places = maze.GetAvailableNeighbours(p.Position, dir);
            int num = places.Count;
            if (num == 0)
                throw new Exception("Nowhere to go");

            for(int i=0; i<places.Count; i++)
            {
                if (places[i].CanEnter())
                {
                   
                    p.Position = places[i].Position;
                }
            }

            
        }

        /// <summary>
        /// Is called to exit the portal without calling the move method
        /// which could crash the game
        /// </summary>
        /// <param name="g"></param>
        private void Exiting(Ghost g)
        {
            List<Tile> places = maze.GetAvailableNeighbours(g.Position, g.Direction);
            int num = places.Count;
            if (num == 0)
                throw new Exception("Nowhere to go");

            for (int i = 0; i < places.Count; i++)
            {
                if (places[i].CanEnter())
                {
                    if (places[i].Position.X == g.Position.X + 1)
                        g.Direction = Direction.Right;
                    else if (places[i].Position.X == g.Position.X - 1)
                        g.Direction = Direction.Left;
                    else if (places[i].Position.Y == g.Position.Y - 1)
                        g.Direction = Direction.Up;
                    else
                        g.Direction = Direction.Down;
                    g.Position = places[i].Position;
                }
            }
        }
    }
}
