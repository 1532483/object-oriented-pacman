﻿using Microsoft.Xna.Framework;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PacMan
{
    /// <summary>
    /// Author : Danny Manzato-Tates
    /// </summary>
    public enum Direction {Left, Right, Up, Down, Still}//Represent direction in which entity is moving

    public enum GhostState { Scared, Chase, Released, Zombie}//Represents the state of the ghost

    /// <summary>
    /// The GameState represents all the business classes
    /// </summary>
    public class GameState
    {
        /// <summary>
        /// Variables contain the state of the game.
        /// </summary>
        public Pacman Pacman { get { return this.pacman; } }
        private Pacman pacman;

        public GhostPack Ghostpack { get { return this.ghostpack; } }
        private GhostPack ghostpack;

        public Maze Maze { get { return this.maze; } }
        private Maze maze;

        public Pen Pen { get { return this.pen; } }
        private Pen pen;

        public ScoreAndLives Score { get { return this.score; } }
        private ScoreAndLives score; 


        /// <summary>
        /// Static method that returns a mapped gamestate instance by reading a specified file
        /// </summary>
        /// <param name="string">File path of the game file</param>
        /// <returns>GameState instance</returns>
        public static GameState Parse(string fileContent)
        {
            GameState g = new GameState();
            g.pacman = new Pacman(g);
            g.ghostpack = new GhostPack();
            g.maze = new Maze(); 
            g.pen = new Pen();
            g.score = new ScoreAndLives(g);

            g.score.Lives = 3;

            Ghost gh;
            string[][] parsed = getElements(fileContent);
            
            Tile[,] array = new Tile[parsed[0].Length,parsed.Length];
            List<Vector2> teleporters = new List<Vector2>();
            for (int y=0; y<parsed.Length; y++)
            {
                for(int x=0; x<parsed[0].Length; x++)
                {
                    switch (parsed[y][x])
                    {
                        case "w":
                            array[x, y] = new Wall(x, y);
                            break;
                        case "p":
                            Pellet p = new Pellet(10);
                            p.Collision += g.score.IncrementScore;
                            array[x, y] = new Path(x, y, p);
                            break;
                        case "e":
                            Energizer e = new Energizer(g.Ghostpack, 100);
                            e.Collision += g.score.IncrementScore;
                            array[x, y] = new Path(x, y, e);
                            break;
                        case "m":
                            array[x, y] = new Path(x,y,null);
                            break;
                        case "x":
                            array[x, y] = new Path(x,y,null);
                            g.pen.AddTile(array[x, y]);
                            break;
                        case "P":
                            array[x, y] = new Path(x, y,null);
                            g.pacman.Position = new Vector2(x,y);
                            g.pacman.initPosition = new Vector2(x, y);
                            break;
                        case "t":
                            teleporters.Add(new Vector2(x, y));
                            break;
                        case "1":
                            gh = new Ghost(g,x,y,g.pacman.Position,GhostState.Chase, new Color(208,62,25));
                            gh.Points = 200;
                            gh.Identifier = parsed[y][x];
                            Ghost.ReleasePosition = new Vector2(x,y);
                            gh.Collision += g.score.IncrementScore;
                            gh.PacmanDied += g.score.DeadPacman;
                            g.ghostpack.Add(gh);
                            array[x, y] = new Path(x, y, null);
                            break;
                        case "2":
                            gh = new Ghost(g, x, y, g.pacman.Position, GhostState.Chase, new Color(234, 130, 229));
                            gh.Points = 200;
                            gh.Identifier = parsed[y][x];
                            gh.Collision += g.score.IncrementScore;
                            gh.PacmanDied += g.score.DeadPacman;
                            g.ghostpack.Add(gh);
                            array[x, y] = new Path(x, y, null);
                            g.pen.AddTile(array[x, y]);
                            g.pen.AddToPen(gh);
                            break;
                        case "3":
                            gh = new Ghost(g, x, y, g.pacman.Position, GhostState.Chase, new Color(70, 191, 238));
                            gh.Points = 200;
                            gh.Identifier = parsed[y][x];
                            gh.Collision += g.score.IncrementScore;
                            gh.PacmanDied += g.score.DeadPacman;
                            g.ghostpack.Add(gh);
                            array[x, y] = new Path(x, y, null);
                            g.pen.AddTile(array[x, y]);
                            g.pen.AddToPen(gh);
                            break;
                        case "4":
                            gh = new Ghost(g, x, y, g.pacman.Position, GhostState.Chase, new Color(214, 133, 28));
                            gh.Points = 200;
                            gh.Identifier = parsed[y][x];
                            gh.Collision += g.score.IncrementScore;
                            gh.PacmanDied += g.score.DeadPacman;
                            g.ghostpack.Add(gh);
                            array[x, y] = new Path(x, y, null);
                            g.pen.AddTile(array[x, y]);
                            g.pen.AddToPen(gh);
                            break;
                    }
                }
            }

            if(teleporters.Count == 2)
            {
                Teleporter t1 = new Teleporter((int)teleporters[0].X, (int)teleporters[0].Y, g.maze);
                Teleporter t2 = new Teleporter((int)teleporters[1].X, (int)teleporters[1].Y, g.maze);
                t1.otherTeleporter = t2;
                t2.otherTeleporter = t1;
                array[(int)teleporters[0].X, (int)teleporters[0].Y] = t1;
                array[(int)teleporters[1].X, (int)teleporters[1].Y] = t2;
            }else
            {
                for(int i = 0; i<teleporters.Count; i++)
                {
                    array[(int)teleporters[i].X, (int)teleporters[i].Y] = new Wall((int)teleporters[i].X, (int)teleporters[i].Y);
                }
            }

            g.maze.SetTiles(array);
            g.maze.PacmanWon += g.Score.Win;
            g.ghostpack.SubToGameOver(g.score);
            g.pacman.SubToGhosts();
            return g;
        }

        /// <summary>
        /// Method that parse the .csv file into a jagged array
        /// </summary>
        /// <param name="filePath">The file path of the maze</param>
        private static string[][] getElements(string fileContent)
        {
            string[] s = { "\r\n" };
            string[] stringLines = fileContent.Split(s, StringSplitOptions.RemoveEmptyEntries);
            string[][] parseStr = new string[stringLines.Length][];
            for (int i=0; i<stringLines.Length; i++)
            {
                parseStr[i] = stringLines[i].Split(',');
            }

            return parseStr;
        }

    }
}
