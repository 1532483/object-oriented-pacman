﻿using Microsoft.Xna.Framework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Collections;
using System.Timers;

namespace PacMan
{
    /// <summary>
    /// Author : Danny Manzato-Tates
    /// </summary>
    public class GhostPack : IEnumerable<Ghost>
    {
        private List<Ghost> ghosts;
        private bool isGameOver = false;//used to not release the penned ghost when the game is over

        /// <summary>
        /// Constructor that instantiates a new List of ghosts.
        /// </summary>
        public GhostPack()
        {
            ghosts = new List<Ghost>();
        }

        /// <summary>
        /// Checks if there is a collision with ghosts
        /// </summary>
        /// <param name="v">Vector in which the ghost is going</param>
        public bool CheckCollideGhosts(Vector2 v)
        {
            foreach(var g in ghosts)
            {
                //Checks to see if each ghosts position matches the V (pacmans position)
                if(g.Position == v)
                {
                    if(g.CurrentState == GhostState.Chase)
                    {
                        ResetGhosts();
                    }

                    g.Collide();
                    return true;
                    //Console.WriteLine("Pacman collides with ghost");
                }
            }
            return false;
        }

        public void SubToGameOver(ScoreAndLives s)
        {
            s.GameOver += (x) => { isGameOver = true; ResetGhosts(); };
        }

        /// <summary>
        /// Calls reset and puts all the ghosts in Pen.
        /// </summary>
        public void ResetGhosts()
        {
            foreach (Ghost g in ghosts) {
                g.ChangeState(GhostState.Chase);
                g.Reset(isGameOver);
            }
        }

        /// <summary>
        /// Sets all the ghosts to scared state
        /// </summary>
        public void ScareGhosts()
        {

            foreach (Ghost g in ghosts)
            {
                g.ChangeState(GhostState.Scared);
            }

            if (Ghost.scared == null)
            {
                Ghost.scared = new Timer(8000);
                Ghost.scared.Elapsed += DisableScared;
                Ghost.scared.Start();
            }
            else
            {
                Ghost.scared.Enabled = false;
                Ghost.scared = new Timer(8000);
                Ghost.scared.Elapsed += DisableScared;
                Ghost.scared.Start();
                this.count = 0;
            }
        }

        /// <summary>
        /// When the timer is off, all ghosts turn back to chase state
        /// </summary>
        /// <param name="sender">The object that holds the timer</param>
        /// <param name="e">The timer event</param>
        private int count = 0;
        private void DisableScared(object sender, ElapsedEventArgs e)
        {
            if (this.count == 1)
            {
                //Console.WriteLine("Second interval trigger and count:" + count);
                Timer t = (Timer)sender;
                t.Enabled = false;

                foreach (var g in ghosts) {
                    g.ChangeState(GhostState.Chase);
                }

                Ghost.scared = null;
                this.count = 0;
            }
            //Console.WriteLine("First interval dont trigger and count: " + count);
            count++;
        }

        /// <summary>
        /// Tells the ghosts where to move
        /// </summary>
        public void Move()
        {
            foreach (Ghost g in ghosts)
            {
                g.Move();
               CheckCollideWithPacman(g.Position, g.Pacman.Position, g);
            }
        }

        private void CheckCollideWithPacman(Vector2 position, Vector2 pacPosition, Ghost g)
        {
            if (position == pacPosition)
            {
                if (g.CurrentState == GhostState.Chase)
                {
                    ResetGhosts();
                }
                g.Collide();
                //Console.WriteLine("Ghost collides with pacman");
            }
        }


        /// <summary>
        /// Adds ghosts to the List
        /// </summary>
        /// <param name="g">The ghost to be added to the list</param>
        public void Add(Ghost g)
        {
            this.ghosts.Add(g);
        }

        /// <summary>
        /// Implements the IEnumerable
        /// </summary>
        /// <returns name="Enumerator"> Ghosts enumerators</returns>
        public IEnumerator<Ghost> GetEnumerator()
        {
            return ghosts.GetEnumerator();
        }
        /// <summary>
        /// Implements the IEnumerable
        /// </summary>
        /// <returns name="Enumerator"> Ghosts enumerators</returns>
        IEnumerator IEnumerable.GetEnumerator()
        {
            return ghosts.GetEnumerator();
        }
    }
}
