﻿using Microsoft.Xna.Framework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PacMan
{
    public class Zombie : IGhostState
    {
        Ghost ghost;
        Vector2 target;
        Maze maze;


        /// <summary>
        /// Constructor instantiates the ghost, maze and target(the pen).
        /// </summary>
        public Zombie(Ghost ghost, Vector2 target, Maze maze)
        {
            this.ghost = ghost;
            this.target = target;
            this.maze = maze;
        }

        /// <summary>
        /// Calculates the shortest distance for the ghosts to get to the pen
        /// similar to chase mode but instead of chasing pacman, it chases the pen
        /// </summary>
        public void Move()
        {
            Tile current = maze[(int)ghost.Position.X, (int)ghost.Position.Y];
            List<Tile> places = maze.GetAvailableNeighbours(ghost.Position, ghost.Direction);
            int num = places.Count;
            if (num == 0)
                throw new Exception("Nowhere to go");

            Tile shortestDistance = places[0];
            for (int i = 1; i < places.Count(); i++)
            {
                if (places[i].GetDistance(target) < shortestDistance.GetDistance(target))
                {
                    shortestDistance = places[i];
                }
            }

            if (shortestDistance.Position.X == ghost.Position.X + 1)
                ghost.Direction = Direction.Right;
            else if (shortestDistance.Position.X == ghost.Position.X - 1)
                ghost.Direction = Direction.Left;
            else if (shortestDistance.Position.Y == ghost.Position.Y - 1)
                ghost.Direction = Direction.Up;
            else
                ghost.Direction = Direction.Down;

            ghost.Position = shortestDistance.Position;
        }
    }
}
