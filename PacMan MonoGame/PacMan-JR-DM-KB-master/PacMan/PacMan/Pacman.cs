﻿using Microsoft.Xna.Framework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PacMan
{
    /// <summary>
    /// Author : Danny Manzato-Tates
    /// </summary>
    public class Pacman
    {
        /// <summary>
        /// Variables and indexer
        /// </summary>
        private GameState controller;
        private Maze maze;

        public Vector2 Position { get { return pos; } set { pos = value; } }
        private Vector2 pos;

        public int Frame { get; set; }
        public Vector2 initPosition;

        /// <summary>
        /// Constructor that instantiates the controller and maze
        /// </summary>
        /// <param name="controller">The controller to be instantianted</param>
        public Pacman(GameState controller)
        {
            this.controller = controller;
            this.maze = this.controller.Maze;
            //added event handlers to reset pacman position
            this.controller.Score.GameOver += (x) => pos = initPosition;
            this.controller.Maze.PacmanWon += (x) => pos = initPosition;
            Frame = 0;
        }

        public void SubToGhosts()
        {
            foreach (var g in this.controller.Ghostpack)
            {
                g.PacmanDied += () => { pos = initPosition; };
            }
        }


        /// <summary>
        /// Checks if pacman can move in a given direction and checks for
        /// collisions
        /// </summary>
        /// <param name="dir">The direction to move in</param>
        /// <returns>Reutrns a boolean of whether it successfully moved</returns>
        public bool Move (Direction dir)
        {
            int x = (int)pos.X;
            int y = (int)pos.Y;

            Tile up = maze[x, y - 1];
            Tile down = maze[x, y + 1];
            Tile left = maze[x - 1, y];
            Tile right = maze[x + 1, y];

            switch (dir)
            {
                case Direction.Up:
                    if(up.CanEnter())
                    {
                        pos = up.Position;
                        if (up is Teleporter)
                        {
                            ((Teleporter)up).Entering(this, Direction.Up);
                        }
                        CheckCollisions();
                        return true;
                    }else
                    {
                        return false;
                    }
                case Direction.Down:
                    if (down.CanEnter())
                    {
                        pos = down.Position;
                        if (down is Teleporter)
                        {
                            ((Teleporter)down).Entering(this, Direction.Down);
                        }
                        CheckCollisions();
                        return true;
                    }
                    else
                    {
                        return false;
                    }
                case Direction.Left:
                    if (left.CanEnter())
                    {
                        pos = left.Position;
                        if (left is Teleporter)
                        {
                            ((Teleporter)left).Entering(this, Direction.Left);
                        }
                        CheckCollisions();
                        return true;
                    }
                    else
                    {
                        return false;
                    }
                case Direction.Right:
                    if (right.CanEnter())
                    {
                        pos = right.Position;
                        if (right is Teleporter)
                        {
                            ((Teleporter)right).Entering(this, Direction.Right);
                        }
                        CheckCollisions();
                        return true;
                    }
                    else
                    {
                        return false;
                    }
            }

            return false;
        }

        /// <summary>
        /// Checks for collisions
        /// </summary>
        public void CheckCollisions()
        {

            controller.Ghostpack.CheckCollideGhosts(Position);

            maze[(int)pos.X, (int)pos.Y].Collide(); 
        }

    }
}
