﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Audio;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework.Media;
using PacMan;

namespace MonoPacmanTest
{
    public class PacmanSprite : DrawableGameComponent
    {
        private SpriteBatch spriteBatch;
        private Texture2D pacmanImage;

        private List<Texture2D> up;
        private List<Texture2D> down;
        private List<Texture2D> left;
        private List<Texture2D> right;

        private Game1 game;
        private GameState gameState;
        private int thresholdMove;
        private int thresholdFrame;
        private int count;
        private bool isGameOver = false;
        private int pixel;
        

        public PacmanSprite(Game1 game, GameState gameState, int pixel) : base(game)
        {
            this.game = game;
            this.gameState = gameState;
            gameState.Score.GameOver += (x) => isGameOver = true; 
            this.thresholdMove = 10;
            this.thresholdFrame = 12;
            this.pixel = pixel;
        }

        public override void Initialize()
        {
            up = new List<Texture2D>();
            down = new List<Texture2D>();
            right = new List<Texture2D>();
            left = new List<Texture2D>();
            base.Initialize();
        }

        protected override void LoadContent()
        {
            this.spriteBatch = new SpriteBatch(GraphicsDevice);
            pacmanImage = game.Content.Load<Texture2D>("pacman_close");

            up.Add(pacmanImage);
            up.Add(game.Content.Load<Texture2D>("pacman_up_half"));
            up.Add(game.Content.Load<Texture2D>("pacman_up_full"));

            down.Add(pacmanImage);
            down.Add(game.Content.Load<Texture2D>("pacman_down_half"));
            down.Add(game.Content.Load<Texture2D>("pacman_down_full"));

            right.Add(pacmanImage);
            right.Add(game.Content.Load<Texture2D>("pacman_right_half"));
            right.Add(game.Content.Load<Texture2D>("pacman_right_full"));


            left.Add(pacmanImage);
            left.Add(game.Content.Load<Texture2D>("pacman_left_half"));
            left.Add(game.Content.Load<Texture2D>("pacman_left_full"));
            base.LoadContent();
        }

        public override void Update(GameTime gameTime)
        {
            checkInput();
            base.Update(gameTime);
        }


        int countFrame = 0;
        public override void Draw(GameTime gameTime)
        {
            Direction pacDir = Direction.Still;
            List<Texture2D> pacAnimation = new List<Texture2D>();
            spriteBatch.Begin();

            if(typeDir == "still")
            {
                pacDir = oldDirection;
            }else if(typeDir == "new")
            {
                pacDir = newDirection;
            }
            else if (typeDir == "old")
            {
                pacDir = oldDirection;
            }

            switch (pacDir)
            {
                case Direction.Right:
                    pacAnimation = right;
                    break;
                case Direction.Left:
                    pacAnimation = left;
                    break;
                case Direction.Down:
                    pacAnimation = down;
                    break;
                case Direction.Up:
                    pacAnimation = up;
                    break;
            }

            countFrame++;
            if (countFrame == 0 || countFrame == thresholdFrame / 2 || countFrame == thresholdFrame)
            {
                if (gameState.Pacman.Frame == 0)
                {
                    gameState.Pacman.Frame = 1;
                }
                else if (gameState.Pacman.Frame == 1)
                {
                    gameState.Pacman.Frame = 2;
                }else
                {
                    gameState.Pacman.Frame = 0;
                }
                countFrame = 0;
            }


            if (pacDir == Direction.Still)
            {
                spriteBatch.Draw(pacmanImage, new Rectangle((int)gameState.Pacman.Position.X * pixel, (int)gameState.Pacman.Position.Y * pixel, pixel, pixel), Color.White);
            }else if(typeDir == "still"){
                spriteBatch.Draw(pacAnimation[1], new Rectangle((int)gameState.Pacman.Position.X * pixel, (int)gameState.Pacman.Position.Y * pixel, pixel, pixel), Color.White);
            }
            else if(pacAnimation.Count() == 3)
            {
                spriteBatch.Draw(pacAnimation[gameState.Pacman.Frame], new Rectangle((int)gameState.Pacman.Position.X * pixel, (int)gameState.Pacman.Position.Y * pixel, pixel, pixel), Color.White);
            }
            spriteBatch.End();

            base.Draw(gameTime);
        }

        private Direction oldDirection = Direction.Still;
        private Direction newDirection = Direction.Still;
        private string typeDir = "still";
        private void checkInput()
        {
            if (!isGameOver)
            {

                KeyboardState newState = Keyboard.GetState();
                if (newState.IsKeyDown(Keys.Right))
                {
                    newDirection = Direction.Right;
                }
                else if (newState.IsKeyDown(Keys.Left))
                {
                    newDirection = Direction.Left;
                }
                else if (newState.IsKeyDown(Keys.Down))
                {
                    newDirection = Direction.Down;
                }
                else if (newState.IsKeyDown(Keys.Up))
                {
                    newDirection = Direction.Up;
                }

                count++;
                if (count > thresholdMove)
                {
                    if (newDirection != Direction.Still)
                    {
                        if (gameState.Pacman.Move(newDirection))
                        {
                            oldDirection = newDirection;
                            typeDir = "new";
                        }
                        else
                        {

                            if (!gameState.Pacman.Move(oldDirection))
                            {
                                newDirection = Direction.Still;
                                typeDir = "still";
                            }
                            else
                            {
                                typeDir = "old";
                            }

                        }
                    }
                    count = 0;
                }
            }
        }
    }
}
