﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Audio;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework.Media;
using PacMan;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MonoPacmanTest
{
    public class MazeSprite : DrawableGameComponent
    {

        private SpriteBatch spriteBatch;
        private Texture2D pelletImage;
        private Texture2D energizerImage;
        private Texture2D emptyImage;
        private Texture2D wallImage;
        private Texture2D portalImage;

        private List<Texture2D> energizerAnimation;
        private int frame = 0;
        private int pixel;
        private Game1 game;
        private GameState gameState;

        public MazeSprite(Game1 game, GameState gameState, int pixel) : base(game)
        {
            this.game = game;
            this.gameState = gameState;
            energizerAnimation = new List<Texture2D>();
            this.pixel = pixel;
        }

        public override void Initialize()
        {
            base.Initialize();
        }

        protected override void LoadContent()
        {
            this.spriteBatch = new SpriteBatch(GraphicsDevice);
            pelletImage = game.Content.Load<Texture2D>("pellet");
            wallImage = game.Content.Load<Texture2D>("wall");
            emptyImage = game.Content.Load<Texture2D>("empty");
            portalImage = game.Content.Load<Texture2D>("portal");
            energizerImage = game.Content.Load<Texture2D>("energizer");
            energizerAnimation.Add(energizerImage);
            energizerAnimation.Add(game.Content.Load<Texture2D>("energizer2"));
            energizerAnimation.Add(game.Content.Load<Texture2D>("energizer3"));

            base.LoadContent();
        }

        bool isGameOver = false;
        public override void Update(GameTime gameTime)
        {
            if (!isGameOver)
            {
                gameState.Maze.CheckMembersLeft();
            }
            count++;
            if (count > 10)
            {
                if (frame == energizerAnimation.Count - 1)
                {
                    frame = 0;
                }
                else
                {
                    frame++;
                }
                count = 0;
            }
            base.Update(gameTime);
        }
        int count;
        public override void Draw(GameTime gameTime)
        {

            spriteBatch.Begin();
            count++;
            for(int y=0; y<gameState.Maze.Size; y++)
            {
                for(int x=0; x<gameState.Maze.Size; x++)
                {
                    if(gameState.Maze[x,y] is PacMan.Path)
                    {
                        if(gameState.Maze[x, y].Member is Pellet)
                        {
                            spriteBatch.Draw(pelletImage, new Rectangle(x*pixel,y*pixel,pixel,pixel), Color.White);
                        }
                        else if (gameState.Maze[x, y].Member is Energizer)
                        {
                                spriteBatch.Draw(energizerAnimation[frame], new Rectangle(x * pixel, y * pixel, pixel, pixel), Color.White);
                                
                        }
                        else//empty
                        {
                            spriteBatch.Draw(emptyImage, new Rectangle(x * pixel, y * pixel, pixel, pixel), Color.White);
                        }
                    }else if (gameState.Maze[x, y] is PacMan.Wall)
                    {
                        spriteBatch.Draw(wallImage, new Rectangle(x * pixel, y * pixel, pixel, pixel), Color.White);
                    }else if (gameState.Maze[x, y] is Teleporter)
                    {
                        spriteBatch.Draw(portalImage, new Rectangle(x * pixel, y * pixel, pixel, pixel), Color.White);
                    }
                        
                }
            }

            spriteBatch.End();

            base.Draw(gameTime);
        }

    }
}
