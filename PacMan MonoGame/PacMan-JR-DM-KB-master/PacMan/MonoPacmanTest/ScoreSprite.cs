﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Audio;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework.Media;
using PacMan;
using System.Timers;

namespace MonoPacmanTest
{
    public class ScoreSprite : DrawableGameComponent
    {
        private SpriteFont spriteFont;
        private SpriteBatch spriteBatch;

        private Game1 game;
        private GameState gameState;
        private Timer time;
        private int miliseconds;
        private List<Ghost> listGhost;
        private bool isGameOver = false;
        private string typeOver;
        private Texture2D empty;
        private int pixel;

       // public event Action nextLevel; 

        public ScoreSprite(Game1 game, GameState gameState, int pixel) : base(game)
        {
            this.game = game;
            this.gameState = gameState;
            miliseconds = 0;
            this.pixel = pixel;
            listGhost = new List<Ghost>();
            foreach (var g in gameState.Ghostpack)
            {
                listGhost.Add(g);
            }

            gameState.Score.GameOver += (x) => { isGameOver = true; typeOver = x; };

        }

        public override void Initialize()
        {
            time = new Timer(1);
            time.Elapsed += addTime;
            time.Start();

            base.Initialize();
        }

        private void addTime(object sender, ElapsedEventArgs e)
        {
            miliseconds++;
        }

        protected override void LoadContent()
        {
            this.spriteBatch = new SpriteBatch(GraphicsDevice);
            this.spriteFont = game.Content.Load<SpriteFont>("Game Font");
            this.empty = game.Content.Load<Texture2D>("empty_black");
            base.LoadContent();
        }

        public override void Update(GameTime gameTime)
        {
            if (isGameOver)
            {
                KeyboardState newState = Keyboard.GetState();
                if(newState.IsKeyDown(Keys.Space))
                {
                    game.Exit();
                }
            }

            base.Update(gameTime);
        }

        public override void Draw(GameTime gameTime)
        {

            spriteBatch.Begin();
            if (isGameOver)
            {
                if(typeOver.ToLower() == "win")
                {
                    spriteBatch.Draw(empty, new Rectangle(0, 9 * pixel, gameState.Maze.Size * pixel, 7 * pixel), Color.Black);
                    spriteBatch.DrawString(spriteFont, "  Congratulations!\n      You Won!\n[press space to exit]", new Vector2(6 * pixel, 9 * pixel), Color.Red);
                }
                else if(typeOver.ToLower() == "dead")
                {
                    spriteBatch.Draw(empty, new Rectangle(0, 9 * pixel, gameState.Maze.Size * pixel, 7 * pixel), Color.Black);
                    spriteBatch.DrawString(spriteFont, "   You have died!\n     Game Over\n[press space to exit]", new Vector2(6 * pixel, 9 * pixel), Color.Red);
                }
            }

                spriteBatch.DrawString(spriteFont, "Score: " + gameState.Score.Score, new Vector2(0, gameState.Maze.Size * pixel), Color.White);
                spriteBatch.DrawString(spriteFont, "Lives: " + gameState.Score.Lives, new Vector2(200, gameState.Maze.Size * pixel), Color.White);
                //spriteBatch.DrawString(spriteFont, "Time: " + miliseconds, new Vector2(500, gameState.Maze.Size * pixel), Color.White);

            spriteBatch.End();

            base.Draw(gameTime);
        }
    }
}
