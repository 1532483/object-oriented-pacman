﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using PacMan;
using System.IO;

namespace MonoPacmanTest
{
    /// <summary>
    /// This is the main type for your game.
    /// </summary>
    public class Game1 : Game
    {
        GraphicsDeviceManager graphics;
        SpriteBatch spriteBatch;
        GameState gameState;

        PacmanSprite pacmanSprite;
        GhostsSprite ghostsSprite;
        MazeSprite mazeSprite;
        ScoreSprite scoreSprite;

        int pixel;
        

        public Game1()
        {
            graphics = new GraphicsDeviceManager(this);
            Content.RootDirectory = "Content";
            pixel = 24;
        }

        /// <summary>
        /// Allows the game to perform any initialization it needs to before starting to run.
        /// This is where it can query for any required services and load any non-graphic
        /// related content.  Calling base.Initialize will enumerate through any components
        /// and initialize them as well.
        /// </summary>
        protected override void Initialize()
        {

            string content = File.ReadAllText("levelsPen.csv");
            gameState = GameState.Parse(content);


            graphics.PreferredBackBufferWidth = gameState.Maze.Size * pixel;  // set this value to the desired width of your window
            graphics.PreferredBackBufferHeight = gameState.Maze.Size * pixel + 64;   // set this value to the desired height of your window
            graphics.ApplyChanges();

            mazeSprite = new MazeSprite(this, gameState,pixel);
            Components.Add(this.mazeSprite);

            pacmanSprite = new PacmanSprite(this, gameState, pixel);
            Components.Add(this.pacmanSprite);

            ghostsSprite = new GhostsSprite(this, gameState, pixel);
            Components.Add(this.ghostsSprite);

            scoreSprite = new ScoreSprite(this, gameState, pixel);
            Components.Add(this.scoreSprite);


            gameState.Score.GameOver += (x) =>
            {
                Components.Remove(this.pacmanSprite);

                Components.Remove(this.ghostsSprite);

            };
            
            base.Initialize();
        }

        /// <summary>
        /// LoadContent will be called once per game and is the place to load
        /// all of your content.
        /// </summary>
        protected override void LoadContent()
        {
            // Create a new SpriteBatch, which can be used to draw textures.
            spriteBatch = new SpriteBatch(GraphicsDevice);

            
            
        }

        /// <summary>
        /// UnloadContent will be called once per game and is the place to unload
        /// game-specific content.
        /// </summary>
        protected override void UnloadContent()
        {
            // TODO: Unload any non ContentManager content here
        }

        /// <summary>
        /// Allows the game to run logic such as updating the world,
        /// checking for collisions, gathering input, and playing audio.
        /// </summary>
        /// <param name="gameTime">Provides a snapshot of timing values.</param>
        protected override void Update(GameTime gameTime)
        {
            if (GamePad.GetState(PlayerIndex.One).Buttons.Back == ButtonState.Pressed || Keyboard.GetState().IsKeyDown(Keys.Escape))
                Exit();
            

            base.Update(gameTime);
        }

        /// <summary>
        /// This is called when the game should draw itself.
        /// </summary>
        /// <param name="gameTime">Provides a snapshot of timing values.</param>
        protected override void Draw(GameTime gameTime)
        {
            GraphicsDevice.Clear(Color.Black);
            
            base.Draw(gameTime);
        }
    }
}
