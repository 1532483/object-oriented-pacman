﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Audio;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework.Media;
using PacMan;

namespace MonoPacmanTest
{
    public class GhostsSprite : DrawableGameComponent
    {
        private SpriteBatch spriteBatch;
        private Texture2D ghost1;
        private Texture2D ghost2;
        private Texture2D eyesUp;
        private Texture2D eyesDown;
        private Texture2D eyesLeft;
        private Texture2D eyesRight;
        private Texture2D scaredFace;

        private List<Texture2D> moveGhost;

        private Game1 game;
        private GameState gameState;
        private int threshold;
        private int count;
        private bool isGameOver = false;
        private int pixel;

        public GhostsSprite(Game1 game, GameState gameState, int pixel) : base(game)
        {
            this.game = game;
            this.gameState = gameState;
            this.threshold = 15;
            this.pixel = pixel;
            this.gameState.Score.GameOver += (x) => isGameOver = true;
        }

        public override void Initialize()
        {
            base.Initialize();
        }

        protected override void LoadContent()
        {
            this.spriteBatch = new SpriteBatch(GraphicsDevice);
            ghost1= game.Content.Load<Texture2D>("ghost_blank_1");
            ghost2 = game.Content.Load<Texture2D>("ghost_blank_2");
            moveGhost = new List<Texture2D>();
            moveGhost.Add(ghost1);
            moveGhost.Add(ghost2);
            eyesUp = game.Content.Load<Texture2D>("eyes_up");
            eyesDown = game.Content.Load<Texture2D>("eyes_down");
            eyesLeft = game.Content.Load<Texture2D>("eyes_left");
            eyesRight = game.Content.Load<Texture2D>("eyes_right");
            scaredFace = game.Content.Load<Texture2D>("scared_face");


            base.LoadContent();
        }

        public override void Update(GameTime gameTime)
        {
            if (!isGameOver)
            {
                count++;

                if (count > threshold)
                {

                    gameState.Ghostpack.Move();
                    count = 0;
                }
            }                 

            base.Update(gameTime);
        }

        public override void Draw(GameTime gameTime)
        {
            
            spriteBatch.Begin();
            Color c;
            Texture2D dirEyes = eyesUp;
            foreach (Ghost g in gameState.Ghostpack)
            {
                

                if (count == threshold / 2 || count == threshold)
                {
                    if (g.Frame == 0)
                    {
                        g.Frame = 1;
                    }
                    else
                    {
                        g.Frame = 0;
                    }
                }

                

                switch (g.Direction)
                {
                    case Direction.Right:
                        dirEyes = eyesRight;
                        break;
                    case Direction.Left:
                        dirEyes = eyesLeft;
                        break;
                    case Direction.Down:
                        dirEyes = eyesDown;
                        break;
                    case Direction.Up:
                        dirEyes = eyesUp;
                        break;
                }

                if (g.CurrentState == GhostState.Scared)
                {
                    c = Color.Blue;
                    dirEyes = scaredFace;
                }
                else
                {
                    c = g.Colour;
                }

                if (!(g.CurrentState == GhostState.Zombie))
                {
                    spriteBatch.Draw(moveGhost[g.Frame], new Rectangle((int)g.Position.X * pixel, (int)g.Position.Y * pixel, pixel, pixel), c);
                }else
                {
                    spriteBatch.Draw(moveGhost[g.Frame], new Rectangle((int)g.Position.X * pixel, (int)g.Position.Y * pixel, pixel, pixel), Color.Black);
                }
                    spriteBatch.Draw(dirEyes, new Rectangle((int)g.Position.X * pixel, (int)g.Position.Y * pixel, pixel, pixel), Color.White);
                
            }

            spriteBatch.End();

            base.Draw(gameTime);
        }
    }
}
